# CSS


## Fundamentals

- [MDN: CSS Properties Inheritance](https://developer.mozilla.org/en-US/docs/Web/CSS/inheritance#inherited_properties)
- [MDN: CSS Pseudo-classes](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes)
- [MDN: CSS Pseudo-elements](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-elements)
- [MDN: CSS Selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)
- [MDN: CSS Specificity](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity)
- [Specifishity PDF Cheatsheet](https://specifishity.com/specifishity.pdf)
- [Universal values (inherit, initial, unset, revert)](https://css-tricks.com/inherit-initial-unset-revert/)


## Spacing

- [Margin Collapsing](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Mastering_margin_collapsing)


## Layouts

- [Flexbox Cheatsheet](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [Game: Flexbox Froggy](https://flexboxfroggy.com/)
- [Game: Grid Garden](https://cssgridgarden.com/)


## Some properties

| property | caniuse |
|----------|---------|
| [aspect-ratio](https://developer.mozilla.org/en-US/docs/Web/CSS/aspect-ratio) | https://caniuse.com/mdn-css_properties_aspect-ratio |
| [clamp](https://developer.mozilla.org/en-US/docs/Web/CSS/clamp) | https://caniuse.com/?search=clamp |
| [gap](https://developer.mozilla.org/en-US/docs/Web/CSS/gap), [row-gap](https://developer.mozilla.org/en-US/docs/Web/CSS/row-gap), [column-gap](https://developer.mozilla.org/en-US/docs/Web/CSS/column-gap) | - |

