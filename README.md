# The web cheatsheet

This repo aims to be a resource for learning details about things such as CSS
and JS without going into the basics because I assume you are already
acquainted with the fundamentals.

The guides are written for people like "I already know this stuff but I'd like
to know the every detail of it".

